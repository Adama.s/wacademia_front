import React from 'react';
import logo from './wacademia-logo.png';
import './App.css';
import img from './img/img-wacademia.png';
import logolog from './img/logo-wacademia2.png';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Animated} from "react-animated-css";

function Index() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
function App() {
  return (
    
    <div className="App">
      <header className="App-header ">
        <div className="logo">
        <img src={logo} className="App-logo" alt="logo" /><Animated animationIn="bounceInLeft" animationOut="fadeOut" isVisible={true}>
    <div>
        <h4 className="bienvenue">Bienvenue sur le réseau étudiant français</h4>
        
    </div>
    
</Animated>
        <button className="plus">En savoir plus</button>
       </div>
       <div><h1 className="title">WACADEMIA</h1><img src={img} className="img-body"></img></div>
      </header>
      
      <div className="log"> 
      <form>
        <center><h1 className="wac-title">WACADEMIA</h1></center>
        <center><h2 className="login">Connexion</h2></center>
        <input type="text" placeholder="E-mail" className="champs"></input><br /><br />
        <input type="password" placeholder="Mot de passe" className="champs"></input>
        <br /><br />
        <input type="submit" ></input>
        <div className="f-l">
          <div><a href="/register" className="footer-login">Mot de passe oublié</a></div>


          <div><a href=""className="footer-login">Inscription</a></div>
        </div>
      </form>
      </div>
      {/* <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about/">About</Link>
            </li>
            <li>
              <Link to="/users/">Users</Link>
            </li>
          </ul>
        </nav>

        <Route path="/" exact component={Index} />
        <Route path="/about/" component={About} />
        <Route path="/users/" component={Users} />
      </div>
    </Router> */}
    </div>
  );
}

export default App;
